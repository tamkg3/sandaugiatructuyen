$().ready(()=>{
   

    $('.table').on('click','button',(event)=>{
        // const id = event.target.
        getModal()
    });
  });
  
function getModal(id){
    return `
<div class="modal fade" data-id="${id}" id="modalLoginForm tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header text-center"> <h4 class="modal-title w-100 font-weight-bold">Đánh giá người thắng </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body mx-3">
    <form action="/sellers/products/${id}/rate" method="post" >
        <div class="md-form mb-5">
      
            <div class="form-group">
                <label>Cho điểm: </label>
                <div class="input-group row">
                    <div class="col-md-6">
                        <i class="fa fa-thumbs-up"></i>
                        <input type="radio" name="like" value="true">
                    </div>
                    <div class="col-md-6">
                        <i class="fa fa-thumbs-down"></i>
                        <input type="radio" name="like" value="false" >                   
                     </div>
                </div>
            </div>
            <div class="form-group">
                <label>Nhận xét: </label>
                <div class="input-group row">
                   <input class="form-control validate" name="comment">
                </div>
            </div>            
     
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button type="button" class="btn btn-default" id="#rating" data-dismiss="modal">Xác nhận</button>
      </div>   
    </form>
   
</div>
</div>
</div>
</div>

                `
            };