var express = require('express');
var router = express.Router();

const { Product } = require('../../models/product')
const { Bidder } = require('../../models/bidder')
const { User } = require('../../models/user')
var ObjectId = require('mongodb').ObjectId; 

const auth = require('../../middleware/auth')
const seller = require('../../middleware/seller')
const sellerController = require('../../controllers/seller')

const { upload } = require('../../configs/fileUpload')
const cpUpload = upload.fields([
    { name: 'main_img', maxCount: 1 },
    { name: 'files', maxCount: 8 }
])

router.get('/', auth, seller, sellerController.get);
router.post('/products/post', auth, seller, cpUpload, sellerController.postProduct)
router.get('/products/getList', auth, seller,sellerController.getProductList);
router.get('/products/getList/filter', auth, seller, sellerController.getProductListAndFilter);

// router.get('/api/check_for_bid/:idProduct/:idBidder', sellerController.checkForBid)

router.get('/products/:ID', auth, seller, async function (req, res) {
    var productID = req.params.ID;
    var productdetail = await Product.findOne({
        _id: productID
    }).populate('seller')
        .populate({
            path: 'bidders.bidder',
            populate: {
                path: 'user'
            }
        })
    res.render('pages/seller/productDetail', {
        product: productdetail
    })
})

router.get('/:ID1/ban/:ID2', auth, seller, async function (req, res) {
    var productID = req.params.ID1;
    var bidderBanID = req.params.ID2;
    var product = await Product.findById({ _id: productID }).exec((err, result) => {
        if (result) {
            for (var i = 0; i < result.bidders.length; i++) {
                if (bidderBanID == result.bidders[i].bidder) {
                    result.bidders[i].isWaitingSeller = true;
                    result.save();
                }
            }
        }
    }); 
    await sleep(5000);
    res.redirect(`/sellers/products/${productID}`);
})

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
router.post('/comment/:winnerId/like', async function(req, res) {
    
    let winnerId = req.params.winnerId;
    let comment = req.body.description;
    console.log('reqqqqq', winnerId, comment);
    let winnerIdObj = new ObjectId(winnerId);
    try {
        await Bidder.updateOne({"user": winnerIdObj}, { $addToSet: {response: {comment},}}, { safe: true }, (err, doc) => {
            if (err) console.log(err);
        });
    } catch(e) {
        console.log('error in find bidder', e.message);
    }
    let bidder=await Bidder.find({"user":winnerIdObj});
    let Like=bidder[0].like+1;
    await Bidder.update({
        "user":winnerIdObj},
        {
            $set:{like:Like}
        }
    )
    res.redirect('/sellers/products/getList');
})

router.post('/comment/:winnerId/dislike', async function(req, res) {
    
    let winnerId = req.params.winnerId;
    let comment = req.body.description;
    console.log('reqqqqq', winnerId, comment);
    let winnerIdObj = new ObjectId(winnerId);
    try {
        await Bidder.updateOne({"user": winnerIdObj}, { $addToSet: {response: {comment},}}, { safe: true }, (err, doc) => {
            if (err) console.log(err);
        });
    } catch(e) {
        console.log('error in find bidder', e.message);
    }
    let bidder=await Bidder.find({"user":winnerIdObj});
    let Dislike=bidder[0].dislike+1;
    await Bidder.update({
        "user":winnerIdObj},
        {
            $set:{dislike:Dislike}
        }
    )
    res.redirect('/sellers/products/getList');
})


router.post('/products/:ID', auth, seller, async function (req, res) {
    var productID = req.params.ID;
    var productdetail = await Product.find({
        _id: productID
    });
    var testvalue = req.body.description;
    var date = new Date;
    today = formatDate(date);
    var Newdescription = productdetail[0].fullDes + '<br>' + today + '</br>' + testvalue;
    Product.findById({ _id: productID }, (err, product) => {
        if (err) res.json(err)
        else {
            if (product == null || product == undefined) {
                res.json('not found')
            } else {
                product.fullDes = product.fullDes + today + testvalue;
                product.save({}, (err, updatedProduct) => {
                    res.redirect(`/sellers/products/${productID}`);

                });
            }
        }
    })
})

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [day, month, year].join('-');
}


module.exports = router;