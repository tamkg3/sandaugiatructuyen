const express = require('express');
const category = express.Router();

const guestCategoryController = require('../../controllers/guest/category/category');

category.get('/', guestCategoryController.category)

category.get('/api/list', guestCategoryController.list)


module.exports = category;