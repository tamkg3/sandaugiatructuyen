var express = require('express');
var categories = express.Router();
const manager = require('../../controllers/admin/categories/manager')

const auth = require('../../middleware/auth')
const admin = require('../../middleware/admin')

categories.get('/', auth, admin, function (req, res, next) {
    res.render('pages/admin/categories');
});

categories.post("/create", manager.create)
categories.get("/read", manager.read)
categories.post("/delete", manager.detete)
categories.post("/update", manager.update)

module.exports = categories;