const {
    Product
} = require('../../../models/product')
const dateHelper = require('../../../helpers/nomalizedDate');
const createError = require('http-errors');

exports.read = async (req, res) => {
    try {
        let data = await Product.find({
            $or: [{ active: true }, { active: null }]
        }).populate('category').populate('seller');
        
        // data.map(doc=>{
        //      doc.createdAt = dateHelper(doc.createdAt);
        //      console.log(doc.createdAt);
        // })
        res.render('pages/admin/products', {
            title: "Quản lý sản phẩm",
            products: data
        });
    } catch (err) {
        next(createError({
            errorCode: 404,
            errorMsg: "resource not found"
        }))
    }
}
exports.delete = (req, res, next) => {
    const active = false;
    const productID = req.params.productID;
    Product.findById({ _id: productID }, (err, data) => {
        if (data == null) {
            next(createError({
                errorCode: 404,
                errorMsg: "cant delete because resource does not exist"
            }))
        } else {
            console.log("active " + active);
            data.active = active;
            //them vao de khong bao loi khi update
            data.seller = data.seller || "5dde9e7dfa42cc405e64a4f8";
            data.winner = data.winner || "5dde9e7dfa42cc405e64a4f8";
            data = data.save({}, (err, deleteProduct) => {
                res.redirect('/admin/products');
            });
        }
    });
}
    // res.send({
    //     return_code: 1,
    //     return_data: data,
    //     return_message: "OK."
    // });

exports.readByID = async(req, res,next) => {
    const productID = req.params.productID;
    console.log("Err");
    try{
        let data = await Product.find({ _id: productID }).populate('category').populate('seller');
        console.log(`data: ${data}`);
        if(data.length>0){
            res.json({
                data: data,
                status: "1"
            });
        }else{
            console.log("server internal err")
            res.json({
                
                status: "0"
            })
        }
        
    }catch(err){
        next(err)
    }
    
}

