const { User } = require('../../../models/user')
const { Seller } = require('../../../models/seller')
const { BidderRequest } = require('../../../models/bidderRequest')
const { Bidder } = require('../../../models/bidder')
const moment = require('moment')
moment.locale('vi-VN');
const mongoose = require('mongoose')


module.exports = {
    getUsers: (req, res) => {
        const manageInfo = [ 'username', 'họ tên', 'role', 'email', 'Thao tác'];
        User.find({emailVerified: true}, (err, docs) => {
            const users = docs.map(user => {
                return {
                    role: user.role,
                    id: user._id, 
                    username: user.userName, 
                    fullname: user.fullName, 
                    email: user.email
                    
                }
            })
            res.render('pages/admin/manage_user', {manageInfo, users})
        })
        
    },
    getUserRole:  async (req, res) => {
        const sellers = await Seller.find({}).populate('user').exec()
        const bidderRequest = await BidderRequest.find({}).populate({
            path: 'bidder',
            populate: {
                path: 'user'
            }
        }).exec()
    
        if(!sellers) return res.render('pages/admin/user_role', {
            bidderRequest: bidderRequest
        })
        let updatedSellers = []
        let updatedSeller;
        // Thay đổi thời gian còn hạn
        for(let seller of sellers) {
            try {
                const fromNow = moment(Date.now()).to(seller.sellerExpires)
                const updateOption = {
                    fromNow, 
                    timeOff: (Date.now() - seller.sellerExpires > 0) ? true : false
                }
                updatedSeller = await Seller.findByIdAndUpdate(seller._id, updateOption, {new: true})
                                            .populate('user')
                                            .exec()
                updatedSellers.push(updatedSeller)
            } catch (error) {
                return res.json({error: error})
            }
            
        }
        res.render('pages/admin/user_role', {
            bidderRequest: bidderRequest,
            updatedSellers
        })
    },
    
    apiDeleteUser: (req, res) => {
        const id = req.query.id;
        
        User.remove({_id: id}, (err, doc) => {
            if(!doc.n) return res.json({status: 'khong tim thay no'})
            res.json({status: 'da f12 no'})
        })
        
    },

    apiGetUser: (req, res) => {
        const id = req.query.id;
        
        User.findById(id, (err, doc) => {
            if(!doc) return res.json({status: 'khong tim thay no'})
            res.json({
                status: 'm khong thoat dc dau con trai ^^',
                user: doc
            })
        })
    },
    apiUpdateUser: (req, res) => {
        const id = req.query.id;
        const user = req.body;
        // update ...
        User.findOneAndUpdate(
            {_id: id},
            {...user},
            {new: true},
            (err, doc) => {
                if(err) return res.send('Xử lý bị lỗi')
                res.redirect('/admin/manageUser')
            }
        )
        
    },

    apiUpgradeBidder: async (req, res) => {
        const idUser = req.params.id;
        const bidder = await Bidder.findOne({user: mongoose.Types.ObjectId(idUser)})

        // Xóa bidder trong bảng bidderRequest
        await BidderRequest.findOneAndRemove({'bidder': bidder._id})
        
        // Xóa bidder trong bảng bidder
        await Bidder.findOneAndRemove({user: mongoose.Types.ObjectId(idUser)})
        // Thay đổi role trong User
        await User.findByIdAndUpdate(idUser, {role: 2})

        // Thêm vào bảng seller
        const sellerExpires = Date.now() + 3600000*24*7;
        
        const newSeller = new Seller({
            user: mongoose.Types.ObjectId(idUser),
            sellerExpires,
            dateExpireString: moment(sellerExpires).format("h:mm a, dddd, Do MMMM YYYY"),
            dateStartString: moment().format("h:mm a, dddd, Do MMMM YYYY"),
        })
        newSeller.save((err, doc) => {
            res.json({message: 'success'})
        })
    },

    apiBackBidder: async (req, res) => {
        const idUser = req.params.id;

        // Xóa seller trong bảng Seller
        await Seller.findOneAndRemove({user: mongoose.Types.ObjectId(idUser)})
        // Thay đổi role trong User
        const updatedUser = await User.findByIdAndUpdate(idUser, {role: 0})
        // Thêm vào bảng Bidder
        const newBidder = new Bidder({
            user: mongoose.Types.ObjectId(idUser)
        })
        await newBidder.save();
        res.json({
            status: true,
            message: 'Success'
        })
    },

    apiUpgradeBidderAll: async (req, res) => {
        const bidderRequests = await BidderRequest.find({}).populate('bidder').exec();
        const idOfBidders = bidderRequests.map((bidderRequest) => bidderRequest.bidder._id)
        const idOfUsers = bidderRequests.map((bidderRequest) => bidderRequest.bidder.user)

        // xóa all bảng bidder request
        await BidderRequest.deleteMany({bidder: idOfBidders}, (err, doc) => {})
        // xóa all bảng Bidder
        await Bidder.deleteMany({_id: idOfBidders})
        // update all role -> 2 ở bảng user
        await User.updateMany({_id: idOfUsers}, {role: 2}, (err, doc) => {})

        // insert all vào bảng seller
        const newSellers = idOfUsers.map(idOfUser => {
            const sellerExpires = Date.now() + 3600000*24*7;
            const newSeller = new Seller({
                user: new mongoose.Types.ObjectId(idOfUser),
                sellerExpires,
                dateExpireString: moment(sellerExpires).format("h:mm a, dddd, Do MMMM YYYY"),
                dateStartString: moment().format("h:mm a, dddd, Do MMMM YYYY"),
            })
            return newSeller
        })
        await Seller.insertMany(newSellers, (err, doc) => {})
        
        res.json({message: 'sucsess'}) 
    },

    apiBackBidderAll: async (req, res) => {

        const sellers = await Seller.find({}, (err, doc)=>{})
        const idOfUsers = sellers.map((seller) => seller.user)
        
        // xóa hết seller
        await Seller.deleteMany({user: idOfUsers}, (err, doc) => {})

        // update role -> 0
        await User.updateMany({_id: idOfUsers}, {role: 0}, (err, doc) => {})

        // thêm all vào Bidder
        const newBidders = idOfUsers.map(idOfUser => {
            
            const newBidder = new Bidder({
                user: new mongoose.Types.ObjectId(idOfUser),
            })
            return newBidder
        })
        await Bidder.insertMany(newBidders, (err, doc) => {})

        res.json({message: 'sucsess'}) 
    }

}



