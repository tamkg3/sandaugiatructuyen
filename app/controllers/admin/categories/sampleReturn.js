exports.Ok = (data) => {
    return {
        return_code: 1,
        return_data: data,
        return_message: "OK."
    }
}

exports.Bad = (data) => {
    return {
        return_code: 0,
        return_data: data,
        return_message: "Bad."
    }
}

exports.ParamsErr = (data) => {
    return {
        return_code: 2,
        return_data: {},
        return_message: "Vui lòng tuyền đúng tham số."
    }
}