const { Category } = require("../../../models/category");
const sampleReturn = require("./sampleReturn");

exports.create = (req, res) => {
  var body = req.body;
  if ("name" in body && "parent_category" in body) {
    var category = new Category(req.body);
    category.save(function (err, e) {
      if (err) {
        res.send(sampleReturn.Bad({}));
      } else {
        res.send(sampleReturn.Ok({}));
      }
    });
  } else {
    res.send(sampleReturn.ParamsErr({}));
  }
};

function fill_parent_category_name(category_item) {
  return new Promise((resolve, reject) => {
    if (category_item['parent_category'] != null) {
      Category.findOne({ _id: category_item['parent_category'] }, (err, data) => {
        if (err || data == undefined) {
          console.log(err);
        }
        else {
          // category_item['parent_category_name'] = data['name'];
          ret_item = {
            _id : category_item._id,
            name : category_item.name,
            parent_category : data['name']
          }
        }
        resolve(ret_item);
      });
    }
    else {
      resolve(category_item);
    }
  });
}


exports.read = (req, res) => {
  Category.find({}, async (err, data) => {
    let promiseMaker = []
    if (err) {
      res.send(sampleReturn.Bad({}));
    } else {
      data.forEach(element => {
        promiseMaker.push(fill_parent_category_name(element));
      });
    }
    Promise.all(promiseMaker)
    .then((category) => {
      res.send(sampleReturn.Ok(category))
    });
  });
};

exports.detete = (req, res) => {
  var body = req.body;
  if ("id" in body) {
    re = await Category.findOne({parent_category: body.id})
    console.log('re',re)
    if (!re) {
      Category.deleteOne(
        {
          _id: body.id
        },
        (err, data) => {
          if (err) {
            console.log(err);
            res.send(sampleReturn.Bad({}));
          } else {
            res.send(sampleReturn.Ok({}));
          }
        }
      );
    }
    else {
      res.send(sampleReturn.Bad({}));
    }
    
  } else {
    res.send(sampleReturn.ParamsErr({}));
  }
};

exports.update = (req, res) => {
  var body = req.body;
  if ("id" in body && ("name" in body || "parent_category" in body)) {
    Category.update(
      {
        _id: body.id
      },
      {
        $set: body
      },
      (err, data) => {
        if (err) {
          console.log(err);
          res.send(sampleReturn.Bad({}));
        } else {
          res.send(sampleReturn.Ok({}));
        }
      }
    );
  } else {
    res.send(sampleReturn.ParamsErr({}));
  }
};
