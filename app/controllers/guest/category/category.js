const { Category } = require('../../../models/category');
const mongoose = require('mongoose')

module.exports = {
    category : async (req, res) => {
        const categoriesForMenu = await Category.find({});
        res.render('pages/guest/categories/category',
        {
            category : categoriesForMenu,
            isAuthenticated : req.user
        });
    },
    list: async (req, res) => {
        const category = await Category.find({});
        res.send(category)
    }
}