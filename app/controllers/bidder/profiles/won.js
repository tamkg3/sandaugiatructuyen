const { Category } = require('../../../models/category');
const { Bidder } = require('../../../models/bidder');
const { Product } = require('../../../models/product');


module.exports = {
    index: async function(req, res) {
        const { role, fullName, userName, email } = req.user;
        const categoriesForMenu = await Category.find({});
        const bidder = await Bidder.findOne({user: req.user._id});
        const products = await Product.find(
            {"bidders.bidder": bidder._id}      
        ).populate('bidders.bidder')
        
        let endProducts = products.filter((item) => {
            let maxBidder = item.bidders[item.bidders.length - 1].bidder._id;
            
            return item.isEnd && (maxBidder.toString() == bidder._id.toString())
        })
        
        res.render('pages/bidder/profile/won',
        {
            category : categoriesForMenu,
            products: endProducts,
            isAuthenticated : req.user,
            fullName
        });
    }
}